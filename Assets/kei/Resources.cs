﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Resources : MonoBehaviour
{
    public Text Answer;
    public Button[] bNumber;
    public Button bPlus;
    public Button bMinus;
    public Button bTimes;
    public Button bDivide;
    public Button bEqual;
    public Button bClear;







    // Start is called before the first frame update
    void Start()
    {
        Answer.text = "";

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void InputNumber(string number)
    {
        Answer.text += number;
    }
    public void InputPlus(Text plusButton)
    {
        if (Answer.text == "" || Answer.text.Contains("+"))
        {
            return;
        }
        Answer.text += plusButton.text;
    }
    public void InputMinus(Text minusButton)
    {
        if (Answer.text == "" || Answer.text.Contains("-"))
        {
            return;
        }
        Answer.text += minusButton.text;
    }
    public void InputTimes(Text timesButton)
    {
        if (Answer.text == "" || Answer.text.Contains("×"))
        {
            return;
        }
        Answer.text += timesButton.text;
    }
    public void InputDivide(Text divideButton)
    {
        if (Answer.text == "" || Answer.text.Contains("÷"))
        {
            return;
        }
        Answer.text += divideButton.text;
    }
    public void InputEqual(Text equal)
    {
        if (!Answer.text.Contains("+"))
        {
            if (!Answer.text.Contains("-"))
            {
                if (!Answer.text.Contains("×"))
                {
                    if (!Answer.text.Contains("÷"))
                    {
                        return;
                    }
                    string[] inputStringDivide = Answer.text.Split('÷');
                    double leftNumberDivide = double.Parse(inputStringDivide[0]);
                    int rightNumberDivide = int.Parse(inputStringDivide[1]);

                    if (rightNumberDivide == 0)
                    {
                        return;
                    }

                    double totalDivide = leftNumberDivide / rightNumberDivide;
                    Answer.text = totalDivide.ToString();
                }
                string[] inputStringTimes = Answer.text.Split('×');
                int leftNumberTimes = int.Parse(inputStringTimes[0]);
                int rightNumberTimes = int.Parse(inputStringTimes[1]);

                int totalTimes = leftNumberTimes * rightNumberTimes;
                Answer.text = totalTimes.ToString(); ;
            }
            string[] inputStringMinus = Answer.text.Split('-');
            int leftNumberMinus = int.Parse(inputStringMinus[0]);
            int rightNumberMinus = int.Parse(inputStringMinus[1]);

            int totalMinus = leftNumberMinus - rightNumberMinus;
            Answer.text = totalMinus.ToString(); ;
        }
        string[] inputStringPlus = Answer.text.Split('+');
        int leftNumberPlus = int.Parse(inputStringPlus[0]);
        int rightNumberPlus = int.Parse(inputStringPlus[1]);

        int totalPlus = leftNumberPlus + rightNumberPlus;
        Answer.text = totalPlus.ToString();


    }
    public void InputClear(Text equal)
    {
        Answer.text = "";
    }
}
