﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JsonData;
using UnityEngine.UI;
using System.IO;
using System.Text;
using Utils;

public class Lesson3 : MonoBehaviour
{

    public Button Excution;
    public Text Result;
    public InputField Genre;

    private JsonDataClass jsonDataClass;
    private JsonDataClass[] aryJsonDataClass = new JsonDataClass[] { };

    void Start()
    {
        //jsonファイルを読み取り
        string jsonFileText = File.ReadAllText("Assets/Resources/test01.json");

        //読み取ったjsonファイルを、JsonDataClassの形式で、分析
        //プログラミングで扱えるようにしていく
        jsonDataClass = JsonUtility.FromJson<JsonDataClass>(jsonFileText);

        //実際に扱えるようになっているかテスト
        //jsonDataClass.items[0] ... リア王周りの情報が入ってる
        //jsonDataClass.items[1] ... スパイダーマン周りの情報が入ってる

        //扱い方：
        //リア王のnameを取得したい場合、jsonDataClass.items[0].name
        //スパイダーマンのdescriptionを取得したい場合、jsonDataClass.items[1].description

        Debug.Log(jsonDataClass.items[1].name);


    }

    void Update()
    {

    }

    public void Research(Text Result)
    {
        //初期化
        Result.text = "";

        //変数を宣言
        //jsonデータに該当データがあるかどうか
        bool flgResult = false;

        //検索結果に記載する文言
        string result = "";

        //itemsの数だけfor文を回す
        for (int a = 0; a < jsonDataClass.items.Length; ++a)
        {

            //もし検索したジャンルのデータがjsonファイルにある場合
            if (Genre.text.Contains(jsonDataClass.items[a].genre))
            {

                flgResult = true;

                //二個以上の検索結果が存在する場合
                if(result != "")
                {
                    result = result + "\n\n";
                }

                result = result + "商品名:" + jsonDataClass.items[a].name + "\n説明:" + jsonDataClass.items[a].description;
            }


        }

        if (flgResult)
        {
            Result.text = result;
        }
        else
        {
            Result.text = "見つかりませんでした";
        }

        /*
        if (Genre.text.Contains(jsonDataClass.items[a].genre))
        {

            Result.text += "商品名:" + jsonDataClass.items[a].name + "\n説明:" + jsonDataClass.items[a].description + "\n" + "\n";
        }
        else
        {
            Result.text = "見つかりませんでした";
        }*/
    

    }
}
