﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

namespace JsonData
{

    [Serializable]
    public class JsonDataClass
    {
        public Item[] items; 

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public JsonDataClass() {
            items = new Item[] { };
        }

    }
    
    [Serializable]
    public class Item
    {
        public int id;
        public string name;
        public string description;
        public string genre;

        /// コンストラクタ
        public Item()
        {
            int id = 0;
            string name = "";
            string description = "";
            string genre = "";
        }
    }

}
